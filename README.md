Welcome to the supporting material repository for the Metabloid challenge.

This is the challenge description:

```
Finally we have a Pokemon.
Our very own Metapod!
But we cannot get him to do anything other than harden - it doesn't matter what we ask him to do.

Can you get him to attack?

The attached file may help you understand how he works.

Note that in general, we advise you NOT to run any unknown binary on your system.
We recommend to instead use a virtual machine to do so.
For these challenges we advise you to use a Linux OS inside the virtual machine.
```

The binary in this repo has the flag stripped from it.

In the real scenario you would get this file, and the binary containing the actual flag would be running on a server, requiring you to perform your attack without having a debugger.